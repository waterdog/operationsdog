
import Foundation

open class BaseOperation: Operation {
  
  class func keyPathsForValuesAffectingIsReady() -> Set<String> {
    return [#keyPath(state)]
  }
  
  class func keyPathsForValuesAffectingIsExecuting() -> Set<String> {
    return [#keyPath(state)]
  }
  
  class func keyPathsForValuesAffectingIsFinished() -> Set<String> {
    return [#keyPath(state)]
  }
  
  @objc private enum State: Int {
    case pending
    case ready
    case executing
    case finished
  }
  
  private let stateQueue = DispatchQueue(label: "BaseOperation State")
  private var _state = State.pending
  @objc private var state: State {
    get {
      var storedState: State = .pending
      stateQueue.sync {
        storedState = self._state
      }
      return storedState
    }
    set {
      willChangeValue(forKey: #keyPath(state))
      stateQueue.sync {
        self._state = newValue
      }
      didChangeValue(forKey: #keyPath(state))
    }
  }
  
  override final public var isAsynchronous: Bool {
    return true
  }
  
  override final public var isReady: Bool {
    if super.isReady {
      for condition in conditions {
        if !condition.validate(for: self) {
          return false
        }
      }
      return true
    }
    return false
  }
  
  override final public var isExecuting: Bool {
    return state == .executing
  }
  
  override final public var isFinished: Bool {
    return state == .finished
  }
  
  final override public func start() {
    if !isCancelled {
      state = .executing
      observers.forEach { $0.operationDidStart(self) }
      main()
    }
  }
  
  override final public func main() {
    if !isCancelled {
      execute()
    }
  }
  
  /// This method should be called when the operation has finished executing.
  final public func finish() {
    state = .finished
    observers.forEach { $0.operationDidFinish(self) }
  }
  
  /// This method is where the operation's work should be done.
  /// Do not call `super`.
  open func execute() {
    finish()
  }
  
  // MARK: Conditions
  
  private(set) public var conditions: [OperationCondition] = []
  
  final public func add(_ condition: OperationCondition) {
    conditions.append(condition)
  }
  
  // MARK: - Observers
  
  private(set) var observers: [Observer] = []
  
  final public func add(observer: Observer) {
    observers.append(observer)
  }
  
}
