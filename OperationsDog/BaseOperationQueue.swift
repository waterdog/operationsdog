import Foundation

open class BaseOperationQueue: OperationQueue {
  override open func addOperation(_ op: Operation) {
    if let operation = op as? BaseOperation {
      var dependencies: [Operation] = []
      
      for condition in operation.conditions {
        if let conditionDependency = condition.dependency(for: operation) {
          dependencies.append(conditionDependency)
        }
      }
      
      operation.addDependencies(dependencies)
      
      dependencies.forEach { self.addOperation($0) }
    }
    
    super.addOperation(op)
  }
}

extension Operation {
  func addDependencies(_ dependencies: [Operation]) {
    for dependency in dependencies {
      self.addDependency(dependency)
    }
  }
}
