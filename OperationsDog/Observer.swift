import Foundation

public protocol Observer {
  func operationDidStart(_ operation: BaseOperation)
  func operationDidFinish(_ operation: BaseOperation)
}
