import Foundation

public protocol OperationCondition {
  func validate(for operation: BaseOperation) -> Bool
  func dependency(for operation: BaseOperation) -> Operation?
}
