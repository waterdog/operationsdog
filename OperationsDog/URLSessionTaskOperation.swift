import Foundation

final public class URLSessionTaskOperation: BaseOperation {
  
  enum URLSessionTaskOperationError: Error {
    case networkError
  }
  
  private var task: URLSessionTask?
  let cache: URL
  
  let completionHandler: ((HTTPURLResponse?) -> Void)?
  
  public init(request: URLRequest, cache: URL, completion: ((HTTPURLResponse?) -> Void)? = nil) {
    self.cache = cache
    self.completionHandler = completion
    
    super.init()
    
    self.task = URLSession.shared.dataTask(with: request) { data, response, error in
      if let data = data {
        try? data.write(to: cache)
      }
      self.completionHandler?(response as? HTTPURLResponse)
      self.finish()
    }
  }
  
  override final public func execute() {
    task?.resume()
  }
  
  override public func cancel() {
    super.cancel()
    task?.cancel()
  }
}
