import XCTest
@testable import OperationsDog

class TestOperation: BaseOperation {
  var finishingDelay: TimeInterval = 0
  
  override func execute() {
    Thread.sleep(forTimeInterval: finishingDelay)
    finish()
  }
}

struct FalseCondition: OperationCondition {
  func validate(for operation: BaseOperation) -> Bool {
    return false
  }
  
  func dependency(for operation: BaseOperation) -> Operation? {
    return nil
  }
}

struct TrueCondition: OperationCondition {
  func validate(for operation: BaseOperation) -> Bool {
    return true
  }
  
  func dependency(for operation: BaseOperation) -> Operation? {
    return nil
  }
}

class BaseOperationTests: XCTestCase {
  
  var queue = OperationQueue()
  var operation = TestOperation()
  
  override func setUp() {
    super.setUp()
    queue = OperationQueue()
    operation = TestOperation()
  }
  
  func testOperationIsAsynchronous() {
    XCTAssertTrue(operation.isAsynchronous)
  }
  
  func testOperationStartsWithPendingStatus() {
    let masterOperation = Operation()
    operation.addDependency(masterOperation)
    
    XCTAssertFalse(operation.isReady)
    XCTAssertFalse(operation.isExecuting)
    XCTAssertFalse(operation.isFinished)
  }
  
  func testOperationExecutingAndFinishedAreFalseWhenReady() {
    queue.isSuspended = true
    queue.addOperation(operation)
    XCTAssertTrue(operation.isReady)
    XCTAssertFalse(operation.isExecuting)
    XCTAssertFalse(operation.isFinished)
  }
  
  func testOperationChangesToExecutingWhenIsExecuting() {
    operation.finishingDelay = 0.5
    
    let expect = expectation(description: "")
    operation.completionBlock = { expect.fulfill() }
    queue.addOperation(operation)
    
    Thread.sleep(forTimeInterval: 0.1)
    XCTAssertTrue(operation.isExecuting)
    XCTAssertFalse(operation.isFinished)
    
    waitForExpectations(timeout: 1)
  }
  
  func testOperationChangesToFinishedWhenFinishExecution() {
    operation.finishingDelay = 0.5
    
    let expect = expectation(description: "")
    operation.completionBlock = {
      XCTAssertTrue(self.operation.isFinished)
      XCTAssertFalse(self.operation.isExecuting)
      expect.fulfill()
    }
    queue.addOperation(operation)
    
    waitForExpectations(timeout: 1)
    
    XCTAssertFalse(operation.isExecuting)
    XCTAssertTrue(operation.isFinished)
  }
  
  // MARK: Operation Conditions
  
  func testOperationIsNotReadyIfConditionFail() {
    let condition = FalseCondition()
    operation.add(condition)
    
    queue.addOperation(operation)
    XCTAssertFalse(operation.isReady)
  }
  
  func testOperationIsReadyIfConditionsAreSatisfied() {
    let condition = TrueCondition()
    operation.add(condition)
    
    queue.addOperation(operation)
    XCTAssertTrue(operation.isReady)
  }
  
  func testOperationIsNotReadyIfOneConditionIsNotSatisfied() {
    operation.add(TrueCondition())
    operation.add(FalseCondition())
    queue.addOperation(operation)
    XCTAssertFalse(operation.isReady)
  }
  
}
