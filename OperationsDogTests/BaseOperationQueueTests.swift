import XCTest
@testable import OperationsDog

struct DependencyCondition: OperationCondition {
  let dependency: Operation?
  
  init(dependency: Operation? = nil) {
    self.dependency = dependency
  }
  
  func validate(for operation: BaseOperation) -> Bool {
    return true
  }
  
  func dependencies(for operation: BaseOperation) -> [Operation]? {
    return nil
  }
  func dependency(for operation: BaseOperation) -> Operation? {
    return dependency
  }
}

class BaseOperationQueueTests: XCTestCase {
  
  var queue = BaseOperationQueue()
  var operation = BaseOperation()
  
  override func setUp() {
    super.setUp()
    queue = BaseOperationQueue()
    queue.isSuspended = true
    
    operation = BaseOperation()
  }
  
  func testQueueDontAddDependenciesIfNoDependencyExists() {
    let condition = DependencyCondition()
    operation.add(condition)
    queue.addOperation(operation)
    XCTAssertTrue(operation.dependencies.isEmpty)
  }
  
  func testQueueAddsDependencyIfOneDependencyExists() {
    let dependency = Operation()
    let condition = DependencyCondition(dependency: dependency)
    operation.add(condition)
    queue.addOperation(operation)
    XCTAssertEqual(operation.dependencies.count, 1)
    XCTAssertTrue(operation.dependencies.contains(dependency))
    XCTAssertTrue(queue.operations.contains(dependency))
  }
  
  func testQueueAddsDependenciesIfTwoDependenciesExist() {
    let firstDependency = Operation()
    let secondDependency = Operation()
    
    let firstCondition = DependencyCondition(dependency: firstDependency)
    let secondCondition = DependencyCondition(dependency: secondDependency)
    
    operation.add(firstCondition)
    operation.add(secondCondition)
    queue.addOperation(operation)
    
    XCTAssertEqual(operation.dependencies.count, 2)
    XCTAssertTrue(operation.dependencies.contains(firstDependency))
    XCTAssertTrue(operation.dependencies.contains(secondDependency))
    XCTAssertTrue(queue.operations.contains(firstDependency))
    XCTAssertTrue(queue.operations.contains(secondDependency))
  }
  
  func testQueueAddsOneDependencyIfOnlyOneConditionHasOne() {
    let dependency = Operation()
    let dependencyCondition = DependencyCondition(dependency: dependency)
    let noDependencyCondition = DependencyCondition()
    
    operation.add(dependencyCondition)
    operation.add(noDependencyCondition)
    queue.addOperation(operation)
    
    XCTAssertEqual(operation.dependencies.count, 1)
    XCTAssertTrue(operation.dependencies.contains(dependency))
    XCTAssertTrue(queue.operations.contains(dependency))
  }
  
}
