import Foundation
import XCTest
@testable import OperationsDog

class URLSessionTaskOperationTests: XCTestCase {
  
  func testOperationWillBeMarkedAsFinishedWhenFinishExecuting() {
    // Create test data and move it to the cache to be fetched
    // through a URLRequest
    let testData = "testData".data(using: .utf8, allowLossyConversion: true)!
    let testDataURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("testData-\(UUID())")
    try! testData.write(to: testDataURL)
    
    // Create the URL for the operation's cache
    let testCacheURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("testCache-\(UUID())")
    
    let queue = OperationQueue()
    let request = URLRequest(url: testDataURL)
    let operation = URLSessionTaskOperation(request: request, cache: testCacheURL)
    
    let expect = expectation(description: "")
    operation.completionBlock = {
      expect.fulfill()
    }
    
    queue.addOperation(operation)
    waitForExpectations(timeout: 1, handler: nil)
  }
  
}
